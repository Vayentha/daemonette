//
// Created by Vayentha on 6/22/20.
//

#ifndef DAEMONETTE_VNETTYPES_H
#define DAEMONETTE_VNETTYPES_H


#define SUCCESS				0		/* Success    				*/
#define ENINIT				1		/* Hasn't been Initialized 	*/
#define EGENERR				1		/* General Error			*/
#define ENOTFND				1		/* Not Found error			*/
#define EINVPRG				1		/* Invalid program			*/
#define EINVFNM				1		/* Invalid filename			*/
#define ECMDBUF				1		/* Buffer too small for cmd */
#define EMAXPID				1		/* Max PIDs reached			*/
#define EINVPID				1		/* Invalid PID				*/
#define EINVFD				1		/* Invalid file descriptor	*/
#define EMAXFD				1		/* Max fd used				*/
#define EINVADR				1		/* Invalid address			*/
#define EMXINS				1		/* Max instances reached 	*/
#define EINVCMD				1		/* Invalid command 			*/
#define EINVPAR				1		/* Invalid parameters 		*/
#define EINVIRQ				1		/* Invalid IRQ			    */
#define EEXCKILL			256		/* Exception killed program */



typedef int int32_t;
typedef unsigned int uint32_t;

typedef short int16_t;
typedef unsigned short uint16_t;

typedef unsigned char uint8_t;



typedef struct fileops {
    void (*init)(void);
    int32_t (*read)(int32_t fd, void* buf, int32_t nbytes);
    int32_t (*write)(int32_t fd, const void* buf, int32_t nbytes);
    int32_t (*open)(const uint8_t* filename);
    int32_t (*close)(int32_t fd);
    int32_t (*ioctl)(int32_t fd, uint32_t cmd, void* param);
} fileops_t;

/*
 * This is the file descriptor struct. It contains all of the necessary information
 * for interacting with files. Each of these are initialized within each of the
 * individual drivers.
 *
 */
typedef struct file_desc {
    fileops_t* ops_table;
    uint32_t inode;
    uint32_t file_pos;
    uint32_t flags;
} file_desc_t;

/*
 * This is the Process Control Block struct. It contains all of the necessary information
 * for starting, stopping, and running a process(program).
 */
typedef struct {
    uint32_t pid;						/* Process ID 					  */
    int32_t parent_pid;					/* Process ID of the parent 	  */
    uint32_t ksp;						/* Process KSP 					  */
    uint32_t kbp;						/* Process KBP					  */
    uint32_t entry_addr;				/* Entry address of the process   */
    file_desc_t fd_arr[64];			/* File array					  */
    uint32_t tid;						/* Terminal ID					  */
                                        /* TODO: Signal info			  */
    uint8_t fname[80];			/* Buffer for the file name 	  */
    uint8_t args[80];			/* Buffer for the arguments		  */
} pcb_t;

/*
 * call this for fun stuff
 *
 */
typedef struct encoder
{
    unsigned int proc_num;
    pcb_t * pcb;
    fileops_t * fileops;
}encoder_t;


#endif //DAEMONETTE_VNETTYPES_H
